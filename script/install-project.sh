# !/bin/bash

# Le script doit etre execute a la racine du projet Lumen

# Si le .env n'existe pas, composer sera lance, le .env cree et APP_KEY aussi
if [[ ! -e ".env" ]]; then
	composer install
	cp .env.exemple .env
	chmod -R o+w storage
	# php artisan key:generate
fi
