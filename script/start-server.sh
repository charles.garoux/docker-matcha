# !/bin/bash

#============================= USAGE =========================================================================================
function print_usage {
	echo "usage : $0 launch-type [repo-git]"
}

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Lancement de docker-compose
if [[ -n $(command -v docker-machine) ]]; then
	chmod -R 777 $DIR/../data/www/project/storage #Temporaire (trouver une solution plus propre pour la gestion des droits de docker-machine)
	$DIR/launch.sh -d
elif [[ -n $(command -v docker) ]]; then
	docker-compose up -d
else
	echo "Need docker installed"
	exit
fi

docker exec -i $(docker ps | grep web | cut -d " " -f 1) bash < $DIR/install-project.sh
