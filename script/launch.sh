# !/bin/bash

cd ~/docker/matcha.docker

docker-machine start server
eval $(docker-machine env server)
docker-compose up $1
