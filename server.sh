# !/bin/bash


DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function print_usage {
	echo "usage : $0 launch-type [repo-git]"
}

# En cas d'utilisation de docker-mahcine, recuperation des variable d'environnement pour docker
if [[ -n $(command -v docker-machine) ]]; then
	eval $(docker-machine env server)
fi

# Choix de l'action
if [[ $1 == "start" ]]; then
	$DIR/script/start-server.sh
	ipserver=$(docker-machine ip server)
	echo "SERVER IP: $ipserver"
elif [[ $1 == "stop" ]]; then
	docker-compose stop
elif [[ $1 == "migrate" ]]; then
	docker exec $(docker ps | grep web | cut -d " " -f 1) php artisan migrate:refresh
elif [[ $1 == "hydrate" ]]; then
	docker exec $(docker ps | grep web | cut -d " " -f 1) php artisan db:seed
else
	echo "Bad parameter: $1"
	exit
fi
