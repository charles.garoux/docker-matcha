# Docker du projet Matcha

**Projet outil** fait pour avoir le plus vite possible un serveur de développement temporaire pour le projet [Matcha](https://gitlab.com/charles.garoux/matcha).

## Commande

Pour lancer les conteneurs :
```
./server.sh start
```
Pour arreter les conteneurs :
```
./server.sh stop
```

Pour initialiser/reinitialiser la base de donnees :
```
./server.sh migrate
```

Pour hydrater la base de donnees :
```
./server.sh hydrate
```
> Fonctionne aussi avec docker-machine

## Installer le projet

Placer le contenue du projet dans le repertoire `data/www/project`

Avec git (a la racine du repertoire) :
```
git clone https://gitlab.com/charles.garoux/matcha.git data/www/project
```

## Pour les utilisateurs de docker-machine

Dans le cas ou l'application ne peu pas lire ou ecrire sur fichier a cause de probleme de droits.
Relancer le script de lancement des conteneurs.
> Un conflict entre MacOS et Docker-machine avec les droits du systeme de fichier peu poser probleme.
> La solution utilisé dans le script est temporaire
